#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* USAGE:
   fasta_splitter filename n_frags outprefix
*/

#define MAX_FRAGS 100
#define MAX_LINE_LENGTH 200
#define POS_STEP 1000000

int min(int a, int b);

int main(int argc, char *argv[])
{

  FILE *fp_in, *fp_out[MAX_FRAGS];
  //fpos_t fp_pos[1024], fp_seg_end, fp_current; //take every million reads should be good for almost any file size < 100GB
  long int nl;
  int i, longest, nfiles, n_out, curr_line, seg_ends[1024], curr_seg_end;
  char str[MAX_LINE_LENGTH];
  char filename[1024];
  //long int size;

  if(argc!=4)
    {
      printf("Usage: %s filename n_frags outprefix\n", argv[0]);
      exit(1);
    }

  sscanf(argv[2],"%d", &nfiles);
  printf("nfiles is %d\n", nfiles);
  
  if((fp_in = fopen(argv[1], "r"))==NULL)
    {
      fprintf(stderr, "%s: can't open %s\n", argv[0], argv[1]);
      exit(1);
    } 
  else
    {
      nl=0;
      longest= MAX_LINE_LENGTH - 1;
      /*      while((c=fgetc(fp_in))!=EOF)
	{
	  if(c=='\n') 
	    ++nl;
	    } */
      while(fgets(str, MAX_LINE_LENGTH, fp_in)!= NULL)
	{
	  if(strlen(str)<longest) 
	    {
	    ++nl;
	    } 
	  else
	    {
	      if(str[longest]=='\n')
		++nl;
	      else printf("MAX_LINE_LENGTH insufficient");
	    }
	  if(nl % POS_STEP == 0){
	    //fgetpos(fp_in, fp_pos + nl/POS_STEP-1);
	    seg_ends[nl/POS_STEP-1] = nl;
	    printf("Progress: %ld lines (%ld)\n", nl, nl/POS_STEP);
	  } 
	}
      // fgetpos(fp_in, fp_pos + nl/POS_STEP); //end of file, integer division returns floor
      seg_ends[nl/POS_STEP]=nl;

      printf("%ld lines observed\n", nl);
      printf("%ld segments total\n", nl/POS_STEP+1);
      
      //get ready to start writing out
      rewind(fp_in);
      n_out = min(nfiles, nl/POS_STEP+1);
      printf("%d files will be generated\n", n_out);
      curr_line=0;
      for(i=1; i<=n_out; i++)
	{
	  sprintf(filename, "%s%s.%04d", argv[3], argv[1], i);
	  if((fp_out[i-1]=fopen(filename, "w"))==NULL)
	    {
	      fprintf(stderr, "%s: can't open %s for writing", argv[0], filename);
	      exit(1);
	    }
	  //check if less than segment end pointer
	  //fp_seg_end = fp_pos[(nl/POS_STEP)*i/n_out];
	  curr_seg_end = seg_ends[(nl/POS_STEP)*i/n_out];
	  
	  //fgetpos(fp_in, &fp_current);
	  while(curr_line<curr_seg_end)
	    {
	      fgets(str, MAX_LINE_LENGTH, fp_in);
	      fputs(str, fp_out[i-1]);
	      fgets(str, MAX_LINE_LENGTH, fp_in);
	      fputs(str, fp_out[i-1]);
	      fgets(str, MAX_LINE_LENGTH, fp_in);
	      fputs(str, fp_out[i-1]);
	      fgets(str, MAX_LINE_LENGTH, fp_in);
	      fputs(str, fp_out[i-1]);
	      curr_line+=4;
	      // fgetpos(fp_in, &fp_current);
  	    }

	  printf("end of seg %ld at line %d (%d)\n", (nl/POS_STEP)*i/n_out+1, curr_seg_end, curr_line);
	  
	  fclose(fp_out[i-1]);
	}
      

    }
  
  fclose(fp_in);

  printf("All done\n");
}


int min(int a, int b)
{
  int op=a;

  if(a<b) op=a;
  if(b<a) op=b;
  if(a==b) op=a;
  return op;
}
